import cherrypy
import json

def measure_printer(operation, clientID):

    print(operation)

    if operation=="temperature":
        #I don't know if we have to manage the json measure as a dictionary or as a string (if it's a string we have to do 
        # json.loads(measure) in order to have a dictionary and fill the senML_message)
        measure = "add the method to take the temperature measure from the sensor"

        print(measure)
        #I don't know if .dump raises some errors if measure is a json or if it inteprets as a dictionary
        json.dump(measure, open("res2_1.json", "w"), indent=4)
        
        senML_message = {'bn': clientID+"/temperature", 'e': {'n': measure["resourcename"], 'u': measure["units"], 't': measure["timestamp"], 'v': measure["value"]}}
        return senML_message
    
    elif operation=="humidity":
        measure = "add the method to take the temperature measure from the sensor"

        print(measure)
        json.dump(measure, open("res2_1.json", "w"), indent=4)
        senML_message = {'bn': clientID+"/humidity", 'e': {'n': measure["resourcename"], 'u': measure["units"], 't': measure["timestamp"], 'v': measure["value"]}}
        return senML_message
    
    elif operation=="allSensor":
        measure_temperature = "add the method to take the temperature measure from the sensor"
        measure_humidity = "add the method to take the humidity measure from the sensor"
        measure = {[measure_temperature], [measure_humidity]}

        print(measure)
        json.dump(measure, open("res2_1.json", "w"), indent=4)

        for meas in measure:
            senML_message = {'bn': clientID+meas["resourcename"], 'e': {'n': meas["resourcename"], 'u': meas["units"], 't': meas["timestamp"], 'v': meas["value"]}}
        
        return senML_message
    
    else:
        print("Operation not manageable or wrong path!\n")
        return "Operation not manageable or wrong path!"

class MeasuresWebService(object):
    exposed = True

    def GET(self, *path):
        if len(path)>1:
            return "Wrong path!"

        clientID = "sensor/impi"

        return measure_printer(path[0], clientID)


if __name__=="__main__":

    conf = {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.sessions.on': True,
        }
    }
    cherrypy.tree.mount(MeasuresWebService(), '/', conf)

    cherrypy.config.update({'server.socket_host': '127.0.0.1'})
    cherrypy.config.update({'server.socket_port': 8080})

    cherrypy.engine.start()
    cherrypy.engine.block()

