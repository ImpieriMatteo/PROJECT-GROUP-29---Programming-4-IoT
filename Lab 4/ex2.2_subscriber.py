from MyMQTT import *
import json
import time

class MeasuresReceiver:

    def __init__(self, clientID, broker, port):
        self.client = MyMQTT(clientID, broker, port, self)

    def start (self):
        self.client.start()

    def subscribe(self, topic):
        self.client.mySubscribe(topic)

    def stop (self):
        self.client.stop()

    def notify(self, msg):
        measures = json.loads(msg)

        # We can have to manage the topic /allSensor that gives 2 measures
        for measure in measures:
            clientID = measure['bn']
            resorce_name = measure['n']
            units = measure['u']
            timestamp = measure['t']
            value = measure['v']

            print(clientID+" measured a "+resorce_name+" of "+value+" "+units+" at the time "+timestamp+"\n\n")
        
        print("\n")

if __name__ == "__main__":

    conf = json.load(open("settings.json"))
    broker = conf["broker"]
    port = conf["port"]
    base_topic = conf["baseTopic"]

    MeasureReceiver = MeasuresReceiver("receiver/impi", broker, port)
    
    MeasureReceiver.start()
    MeasureReceiver.subscribe(base_topic+"/group29/sensors/temperature")
    MeasureReceiver.subscribe(base_topic+"/group29/sensors/humidity")
    MeasureReceiver.subscribe(base_topic+"/group29/sensors/allSensors")

    while True:

        #Too much? How can it modify the behaviour of the app?
        time.sleep(5)
        

	MeasureReceiver.client.stop()