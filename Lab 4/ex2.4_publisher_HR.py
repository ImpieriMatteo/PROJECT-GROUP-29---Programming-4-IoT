from MyMQTT import *
import time
import json
import numpy 

class HRSender:

    def __init__(self, clientID, broker, port):
        self.__message={'bn': clientID, 'e': {'n': 'Heart rate', 'u': None, 't': None, 'v': None}}

        self.client=MyMQTT(clientID, broker, port, None)

    def start (self):
        self.client.start()

    def stop (self):
        self.client.stop()

    def publish(self, topic):
        message = self.__message

        shape, scale = 2., 2.  # mean=4, std=2*sqrt(2)
        heart_rate = 5.3*numpy.random.gamma(shape, scale, 1)+50
        message["v"] = heart_rate[0]

        self.client.myPublish(topic, message)
        print("Published")
        

if __name__ == "__main__":

    conf = json.load(open("settings.json"))
    broker = conf["broker"]
    port = conf["port"]
    base_topic = conf["baseTopic"]

    HRSender = HRSender("HRsensor/impi", broker, port)
    HRSender.start()

    
    i = 0
    while i<24:

        HRSender.publish(base_topic+"/group29/sensors/heart_rate")

        i += 1
        time.sleep(5)

    HRSender.client.stop()