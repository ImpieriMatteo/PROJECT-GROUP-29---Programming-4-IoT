from MyMQTT import *
import json
import time

class MeasuresReceiver:

    def __init__(self, clientID, broker, port):
        self.client = MyMQTT(clientID, broker, port, self)

    def start (self):
        self.client.start()

    def subscribe(self, topic):
        self.client.mySubscribe(topic)

    def stop (self):
        self.client.stop()

    def notify(self, msg):
        heart_rate = json.loads(msg)
        hr_Log = json.load(open("hrLog.json"))
        hr_Log["heart_rate log"] = heart_rate
        json.dump(hr_Log, open("hrLog.json", "w"))
        
        print("Measured heart rate: "+str(heart_rate["v"]+"\n"))

if __name__ == "__main__":

    conf = json.load(open("settings.json"))
    broker = conf["broker"]
    port = conf["port"]
    base_topic = conf["baseTopic"]

    HRreceiver = MeasuresReceiver("HRsensor/impi", broker, port)
    
    HRreceiver.start()
    HRreceiver.subscribe(base_topic+"/group29/sensors/heart_rate")

    time.sleep(180)
        
    HRreceiver.client.stop()