import time
import requests

def perform_request():
    url = 'http://127.0.0.1:8080/temperature'

    r = requests.get(url)
    print("\n")
    try:
        #I presume that I can obtain a SenML message and convert it in a json, if we cannot we have to retrieve it as a normal string
        # and then use json.loads(result) to convert it into a dictionary and then use it as it used in 'return'
        #In the last case we have to write a different try except (to handle json.loads())  
        result = r.json()
        print(result["bn"]+" measured a "+result["n"]+" of "+result["v"]+" "+result["u"]+" at the time "+result["t"]+"\n\n")

        return result["v"]

    except:
        result = r.text
        print(result)

if __name__=="__main__":

    data_aquisition = ()
    while True:

        if len(data_aquisition)==10:
            average_temperature = 0

            for temp in data_aquisition:
                average_temperature += temp

            print("Average temperature for the last 10 aquisitions: "+str(average_temperature/len(data_aquisition)))

        data_aquisition = float(perform_request())

        time.sleep(3)

