from MyMQTT import *
import json
import time

class MeasuresReceiver:

    def __init__(self, clientID, broker, port):
        self.client = MyMQTT(clientID, broker, port, self)

    def start (self):
        self.client.start()

    def subscribe(self, topic):
        self.client.mySubscribe(topic)

    def stop (self):
        self.client.stop()

    def notify(self, msg):
        temperature = json.loads(msg)
        
        print("Measured temperature: "+str(temperature["v"]+" "+temperature["u"]+"\n"))
        if temperature>25:
            print("""!!!!!!!!!!!!!! ALERT !!!!!!!!!!!!!!
            TEMPERATURE TOO HIGH\n""")

        elif temperature<10:
            print("""!!!!!!!!!!!!!! ALERT !!!!!!!!!!!!!!
            TEMPERATURE TOO LOW\n""") 

if __name__ == "__main__":

    conf = json.load(open("settings.json"))
    broker = conf["broker"]
    port = conf["port"]
    base_topic = conf["baseTopic"]

    HRreceiver = MeasuresReceiver("TempSensor/impi", broker, port)
    
    HRreceiver.start()
    HRreceiver.subscribe(base_topic+"/group29/sensors/temperature")

    time.sleep(240)
        
    HRreceiver.client.stop()