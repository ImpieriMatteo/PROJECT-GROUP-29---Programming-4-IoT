from MyMQTT import *
import time
import json

class MeasuresSender:

    def __init__(self, clientID, broker, port):
        self.__message={'bn': clientID, 'e': {'n': None, 'u': None, 't': None, 'v': None}}

        self.client=MyMQTT(clientID, broker, port, None)

    def start (self):
        self.client.start()

    def stop (self):
        self.client.stop()

    def publish(self, topic, value):
        message = self.__message
        
        #We have to modify the name of the attributes of value (the measure provided by the sensor) whit the real names
        message['n'] = value["resourcename"]
        message['u'] = value["units"]
        message['t'] = value["timestamp"]
        message['v'] = value["value"]
        self.client.myPublish(topic, message)
        print("Published")

if __name__ == "__main__":

    conf = json.load(open("settings.json"))
    broker = conf["broker"]
    port = conf["port"]
    base_topic = conf["baseTopic"]

    MeasureSender = MeasuresSender("sensor/impi", broker, port)
    MeasureSender.start()

    done = False

    while not done:

        # Take the measures from the sensors as a json file and give them to publish

        # temperature
        MeasureSender.publish(base_topic+"/group29/sensors/temperature")

        # humidity
        MeasureSender.publish(base_topic+"/group29/sensors/humidity")

        # all sensors
        MeasureSender.publish(base_topic+"/group29/sensors/allSensors")

        time.sleep(5)

	MeasureSender.client.stop()