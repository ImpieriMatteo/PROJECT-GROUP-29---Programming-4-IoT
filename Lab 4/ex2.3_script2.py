from MyMQTT import *
import json
import time

humidity = 0.0
measure_num = 0

class HumidityManager:

    def __init__(self, clientID, broker, port):
        self.client = MyMQTT(clientID, broker, port, self)

    def start (self):
        self.client.start()

    def subscribe(self, topic):
        self.client.mySubscribe(topic)

    def stop (self):
        self.client.stop()

    def publish(self, topic, value):
        message = "Average humidity for the last 10 aquisitions: "+str(value)
        
        self.client.myPublish(topic, message)
        print("Published, humidity = "+str(value))

    def notify(self, msg):
        #This part can be erased, we mantain it to debug and see the messages
        #"""
        measure = json.loads(msg)
        clientID = measure['bn']
        resorce_name = measure['n']
        units = measure['u']
        timestamp = measure['t']
        value = measure['v']

        print(clientID+" measured a "+resorce_name+" of "+value+" "+units+" at the time "+timestamp+"\n\n")
        #"""

        # I don't know how to retrieve data from this function (if I understand this function (notify) is triggered automatically
        # when something is posted in the topics followed) so I use a global variable to retrieve the humidity measures
        global humidity
        global measure_num
        humidity += measure["v"]
        measure_num += 1
        

if __name__ == "__main__":

    conf = json.load(open("settings.json"))
    broker = conf["broker"]
    port = conf["port"]
    base_topic = conf["baseTopic"]

    humidityManager = HumidityManager("humidity_manager/impi", broker, port)
    
    humidityManager.start()
    humidityManager.subscribe(base_topic+"/group29/sensors/humidity")

    while True:

        if measure_num==10:
            average_humidity = 0

            for hum in humidity:
                average_humidity += hum
            
            measure_num = 0

            humidityManager.publish(base_topic+"/group29/sensors/average_humidity", average_humidity/len(humidity))

        time.sleep(3)
        

	humidityManager.client.stop()