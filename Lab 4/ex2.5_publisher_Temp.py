from MyMQTT import *
import time
import json
import numpy 

class TemperatureSender:

    def __init__(self, clientID, broker, port):
        self.__message={'bn': clientID, 'e': {'n': 'temperature', 'u': "Cel", 't': None, 'v': None}}

        self.client=MyMQTT(clientID, broker, port, None)

    def start (self):
        self.client.start()

    def stop (self):
        self.client.stop()

    def publish(self, topic, range):
        message = self.__message

        # Look to the links in the slide of Lab 4 to understand why I'm using these distributions to simulate 
        # the temperature measures

        # We can add even the timestamp (using a flag or something that increments through thee iterations) if we want to test 
        # the other applications
        if range=="low":
            temperature = numpy.random.logistic(8, 1, 1)
            message["v"] = temperature[0] # I use the notation [] beacuse the previous function returns a numpy array that we cannot handle using json 
            self.client.myPublish(topic, message)

        elif range=="normal":
            temperature = numpy.random.logistic(18, 0.4, 1)
            message["v"] = temperature[0] 
            self.client.myPublish(topic, message)

        elif range=="high":
            temperature = numpy.random.logistic(26, 1, 1)
            message["v"] = temperature[0]  
            self.client.myPublish(topic, message)

        self.client.myPublish(topic, message)
        print("Published")
        

if __name__ == "__main__":

    conf = json.load(open("settings.json"))
    broker = conf["broker"]
    port = conf["port"]
    base_topic = conf["baseTopic"]

    temperatureSender = TemperatureSender("TempSensor/impi", broker, port)
    temperatureSender.start()

    time.sleep(5)

    while True:

        range = input("""Select a range of temperature to send or QUIT to exit the application ( LOW []; NORMAL []; HIGH [] )
        (The temperature values will be sent every 5 seconds)
        Command: """).lower()

        if range=="low" or range=="normal" or range=="high":
            # can be easily modified to handle every time interval (not default)
            time_range = input("Select the time interval (write only the number) in which the temperature measurements will be sent ( 30 sec, 60 sec, 120 sec ): ")
            print(time_range)
            if time_range!="30" and time_range!="60" and time_range!="120":
                print("Selected a not defined time interval!!\n\n")
            
            else:
                iter = 0
                while iter!=time_range:

                    temperatureSender.publish(base_topic+"/group29/sensors/temperature", range)
                    time.sleep(5)
                    iter += 5

        elif range=="quit":
            print("Thanks for using our service, goodbye!")
            temperatureSender.client.stop()
            break

        else:
            print("Wrong command! Insert a new one")
